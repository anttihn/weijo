Weijo
-----

Weijo is an example client application for [W2E](https://w2e.fi) It uses the [W2E API](https://w2e.fi/doc) to create a user and let the user link some devices.

This is a simple Node.js + Express application.

[Running at Heroku.](https://weijo.herokuapp.com)