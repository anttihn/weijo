var express = require('express');
var router = express.Router();

var HTTPS = process.env.PROTOCOL && process.env.PROTOCOL.toLowerCase() === 'https';
var http = HTTPS ? require('https') : require('http');
var W2E = process.env.W2E;
var APP_ID = process.env.APP_ID;
var APP_SECRET = process.env.APP_SECRET;

router.get('/', function(req, res) {
    res.render('step0', {W2E: W2E});
});

router.get('/step1', function(req, res) {
    res.render('step1', {W2E: W2E});
});

router.post('/step2', function(req, res) {
    var json = {
        client_id: APP_ID,
        client_secret: APP_SECRET
    };

    httpPostJson('/api/organization/users', json, function(data) {
        res.render('step2', {W2E: W2E, data: data});
    },
    function(message) {
        res.render('error', {message: message});
    });
});

router.get('/step3', function(req, res) {
    var username = req.query.username;
    var access_token = req.query.access_token;
    var path = '/api/users/' + username;
    var auth = 'Bearer ' + access_token;

    httpGet(path, auth, function(data) {
        res.render('step3', {W2E: W2E, data: data, username: username, access_token: access_token});
    },
    function(message) {
        res.render('error', {message: message});
    });
});

router.get('/step4', function(req, res) {
    var username = req.query.username;
    var access_token = req.query.access_token;
    var sources = req.query.sources.split(' ');
    var path = '/api/organization/users/' + username + '/link_token';
    var auth = 'Bearer ' + access_token;

    var fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
    var callback = encodeURIComponent(fullUrl);

    httpGet(path, auth, function(data) {
        res.render('step4', {W2E: W2E, data: data, username: username, access_token: access_token, sources: sources, callback: callback});
    },
    function(message) {
        res.render('error', {message: message});
    });
});


router.get('/step5', function(req, res) {
    var username = req.query.username;
    var access_token = req.query.access_token;
    var path = '/api/users/' + username;
    var auth = 'Bearer ' + access_token;

    httpGet(path, auth, function(data) {
        res.render('step5', {W2E: W2E, data: data, username: username, access_token: access_token});
    },
    function(message) {
        res.render('error', {message: message});
    });
});

router.post('/step6', function(req, res) {
    var username = req.body.username;
    var access_token = req.body.access_token;
    var path = '/api/organizations/' + APP_ID + '/users/' + username;
    var auth = 'Bearer ' + access_token;

    httpDelete(path, auth, function() {
        res.render('step6', {W2E: W2E, username: username});
    },
    function(message) {
        res.render('error', {message: message});
    });
});

function httpGet(path, auth, callback, err) {
    var options = {
        host: W2E,
        path: path,
        method: 'GET',
        headers: {
            Authorization: auth
        }
    };

    var _cb = function(response) {
        var chunks = [];
        response.on('data', function(chunk) {
            chunks.push(chunk);
        });
        response.on('end', function() {
            if (response.statusCode >= 200 && response.statusCode < 300) {
                callback(JSON.parse(chunks.join('')));
            }
            else {
                err(response.statusCode);
            }
        });
    };

    http.request(options, _cb).on('error', function(e) {
        err(e);
    }).end();
}

function httpPostJson(path, json, callback, err) {
    var body = JSON.stringify(json);
    var options = {
        host: W2E,
        path: path,
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Content-Length': body.length
        }
    };

    var _cb = function(response) {
        var chunks = [];
        response.on('data', function(chunk) {
            chunks.push(chunk);
        });
        response.on('end', function() {
            if (response.statusCode >= 200 && response.statusCode < 300) {
                callback(JSON.parse(chunks.join('')));
            }
            else {
                err(response.statusCode);
            }
        });
    };

    var req = http.request(options, _cb);
    req.on('error', function(e) {
        err(e);
    });
    req.write(body);
    req.end();
}

function httpDelete(path, auth, callback, err) {
    var options = {
        host: W2E,
        path: path,
        method: 'DELETE',
        headers: {
            'Authorization': auth,
            'Content-Length': '0'
        }
    };

    var _cb = function(response) {
        response.on('data', function() {
            // ...
        });
        response.on('end', function() {
            if (response.statusCode >= 200 && response.statusCode < 300) {
                callback();
            }
            else {
                err(response.statusCode);
            }
        });
    };

    var req = http.request(options, _cb);
    req.on('error', function(e) {
        err(e);
    });
    req.end();
}

module.exports = router;
